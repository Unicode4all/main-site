//	Desna Press	State Agency 2014
//	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
//	use this file except in compliance with the License. You may obtain a copy 
//	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
//	by applicable law or agreed to in writing, software distributed under the 
//	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
//	OF ANY KIND, either express or implied. See the License for the specific 
//	language governing permissions and limitations under the License.

package org.desnapress.model;

public class User {
	
	private long ID;
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword_hash() {
		return password_hash;
	}
	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}
	private String username;
	private String password_hash;

}
