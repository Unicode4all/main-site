//	Desna Press	State Agency 2014
//	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
//	use this file except in compliance with the License. You may obtain a copy 
//	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
//	by applicable law or agreed to in writing, software distributed under the 
//	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
//	OF ANY KIND, either express or implied. See the License for the specific 
//	language governing permissions and limitations under the License.

package org.desnapress.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.desnapress.data.Password;
import org.desnapress.model.User;

@ManagedBean(name = "userController")
@SessionScoped
public class UserController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String password;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// getters and setters
	@Resource(lookup = "java:jboss/datasources/mysql")
	private DataSource src;

	public boolean isLoggedIn() {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get("loginController") != null;
	}

	public String login() throws Exception, SQLException {
		Password pas = new Password();
		String hash;
		hash = pas.hash(getPassword());
		Connection connection;
		User user = new User();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Неверное имя пользователя или пароль!", "Length = 8");

		if (src == null)
			throw new SQLException("Can't create datasource");
		try {
			connection = src.getConnection();

			if (connection == null)
				throw new SQLException("Can't connect to database");

			PreparedStatement statement = connection
					.prepareStatement("SELECT * FROM user");

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				user.setID(result.getLong("id"));
				user.setUsername(result.getString("username"));
				user.setPassword_hash(result.getString("password"));
			}

			connection.close();
			result.close();
		} catch (SQLException e) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							"reg:messages",
							new FacesMessage(
									FacesMessage.SEVERITY_FATAL,
									"Фатальная ошибка:",
									"Невозможно выполнить подключение к базе данных! Обратитесь к системному администратору."));
			return "login";
		}
		;

		if (user.getUsername().equals(name)
				&& user.getPassword_hash().equals(hash))
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("loginController", this);
		else {
			FacesContext.getCurrentInstance().addMessage(
					"reg:messages",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка:",
							"Неверный логин или пароль!"));
			return "login";
		}
		return "login";
	}

	public String logout() {
		if (!isLoggedIn())
			return "index";
		name = null;
		setPassword(null);

		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		session.invalidate();
		return "login";

		// here you wanna forward to your login page

	}
}
